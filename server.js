const Discord = require("discord.js");
const profanity = require('profanity-censor');
const data = require("./config.json");
const client = new Discord.Client();

var currentTime = new Date().toLocaleTimeString();
var prefixTime = new Date().toLocaleString();

profanity.use(data.blacklist);

client.on("ready", () => {
  console.log(`[ BOT ] : ${currentTime} : Logged in as ${client.user.tag}, in ${client.channels.size} channels of ${client.guilds.size} guilds!`);
  console.log('------');
});

client.on("message", async message => {
	if(message.author.bot) return;

	if(data.enableBlacklistRejection == true) {
		if(data.blacklist.some(el => message.content.includes(el)) == true) {
    	return;
    } else if(message.attachments.size > 0) {
  		message.attachments.forEach(function(attachment) {
  			if(message.channel.id == data.channel_1) {
          var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
  				client.channels.get(data.channel_2).send({embed: {
            description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
            image: { url: attachment.url },
            timestamp: new Date(),
            footer: { icon_url: data.footerImageURL, text: data.footerText } }});
  			} else if(message.channel.id == data.channel_2) {
          var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
  				client.channels.get(data.channel_1).send({embed: {
            description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
            image: { url: attachment.url },
            timestamp: new Date(),
            footer: { icon_url: data.footerImageURL, text: data.footerText } }});
  			} else {
  				return;
  			}
      })
    } else {
      if(message.channel.id == data.channel_1) {
        var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
        client.channels.get(data.channel_2).send({embed: {
          description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
          timestamp: new Date(),
          footer: { icon_url: data.footerImageURL, text: data.footerText } }});
      } else if(message.channel.id == data.channel_2) {
        var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
        client.channels.get(data.channel_1).send({embed: {
          description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
          timestamp: new Date(),
          footer: { icon_url: data.footerImageURL, text: data.footerText } }});
      } else {
        return;
      }
    }
  } else {
    if(message.attachments.size > 0) {
  		message.attachments.forEach(function(attachment) {
  			if(message.channel.id == data.channel_1) {
          var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
  				client.channels.get(data.channel_2).send({embed: {
            description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
            image: { url: attachment.url },
            timestamp: new Date(),
            footer: { icon_url: data.footerImageURL, text: data.footerText } }});
  			} else if(message.channel.id == data.channel_2) {
          var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
  				client.channels.get(data.channel_1).send({embed: {
            description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
            image: { url: attachment.url },
            timestamp: new Date(),
            footer: { icon_url: data.footerImageURL, text: data.footerText } }});
  			} else {
  				return;
  			}
      })
    } else {
      if(message.channel.id == data.channel_1) {
        var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
        client.channels.get(data.channel_2).send({embed: {
          description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
          timestamp: new Date(),
          footer: { text: data.footerText } }});
      } else if(message.channel.id == data.channel_2) {
        var cleanMsg = profanity.filter(message.content), regex = /\*\*\*\*\*/gi;
        client.channels.get(data.channel_1).send({embed: {
          description: data.prefix + " " + cleanMsg.replace(regex, '#####'),
          timestamp: new Date(),
          footer: { text: data.footerText } }});
      } else {
        return;
      }
    }
  }
});

client.login(data.token);
