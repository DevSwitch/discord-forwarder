# Discord Chat Mirroring
A discord bot for mirroring chat and images between two channels!


### Setup
* extract files to your system
* create a bot user - [GUIDE](https://bitbucket.org/DevSwitch/discord-forwarder/wiki/Creating%20a%20Discord%20Bot%20User%20and%20Inviting%20It)
* npm i - to install pre-reqs
* edit the config.json
* node server.js - to start the bot

### Config.json
This is the file were you will do all customizing of the bot. The following are the options.

* token - the bot token of the created bot
* prefix - the text to show before the mirrored message
* footerText - the text to add to mirrored chat messages
* footerImageURL - the logo to show with mirrored images
* channel_1 - the ID of the first channel to watch
* channel_2 - the ID of the second channel to watch
* blacklist - an array of words to blacklist
* enableBlacklistRejection - true or false to not mirror messages that contains words on the blacklist

Example config is included in the files and shown below:
```
{
  "token" : "Your_Bot_Token",
  "prefix" : "TEST",
  "footerText" : "Test Footer Text",
  "footerImageURL" : "Footer_Logo_URL_Here",
  "channel_1" : "1st_Channel_ID",
  "channel_2" : "2nd_Channel_ID",
  "blacklist" : [
    "WORD1",
    "WORD2",
    "WORD3"
  ],
  "enableBlacklistRejection" : false
}
```
